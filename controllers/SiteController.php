<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function  actionCrud(){
        return $this->render("gestion");
    }
    public function actionConsulta1() {
      // mediante DAO
                    
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(*) nciclistas FROM ciclista',
            
             ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'], 
            "enunciado"=>"Numero de ciclistas que hay",
             "titulo"=>"Consulta 1",
            "sql"=>"Select distinct from ciclista",
        ]);
        
        
    }
    
    
    
    public function  actionConsulta1a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->distinct()->select("count(*) as nciclistas"),
                ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'], 
            "enunciado"=>"Numero de ciclistas que hay",
             "titulo"=>"Consulta 1",
            "sql"=>"SELECT COUNT(*) AS nciclistas FROM ciclista;",
        ]);
    }
    public function  actionConsulta2(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) nciclista FROM ciclista WHERE nomequipo = 'Banesto'")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(*) nciclista FROM ciclista WHERE nomequipo = "Banesto"',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nciclista'], 
            "enunciado"=>"Numero de ciclistas del equipo Banesto",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT count(*) FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    public function  actionConsulta2a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("count(*) as nciclistas")
                ->where("nomequipo = 'Banesto'"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nciclistas'], 
            "enunciado"=>"Numero de ciclistas del equipo Banesto",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT count(*) FROM ciclista WHERE nomequipo = 'Banesto'';",
        ]);
    }
    
    public function  actionConsulta3(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT avg(edad) edadpromedio FROM ciclista")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT avg(edad) edadpromedio FROM ciclista',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edadpromedio'], 
            "enunciado"=>"Edad media de los ciclistas",
             "titulo"=>"Consulta 3",
            "sql"=>"SELECT avg(edad) edadpromedio FROM ciclista",
        ]);
    }
    public function  actionConsulta3a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("avg(edad) as edadpromedio"),
            'pagination'=>[
                'pageSize'=>0,
            ]
                ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edadpromedio'], 
            "enunciado"=>"Edad media de los ciclistas",
             "titulo"=>"Consulta 3",
            "sql"=>"SELECT avg(edad) edadpromedio FROM ciclista;",
        ]);
    }        
    public function  actionConsulta4(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT avg(edad) edadpromedio FROM ciclista WHERE nomequipo = 'Banesto'")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT avg(edad) edadpromedio FROM ciclista WHERE nomequipo = "Banesto"',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edadpromedio'], 
            "enunciado"=>"Edad media de los ciclistas de Banesto",
             "titulo"=>"Consulta 4",
            "sql"=>"SELECT avg(edad) edadpromedio FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    public function  actionConsulta4a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("avg(edad) as edadpromedio")
                ->where("nomequipo = 'Banesto'"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edadpromedio'], 
            "enunciado"=>"Edad media de los ciclistas de Banesto",
             "titulo"=>"Consulta 4",
            "sql"=>"SELECT avg(edad) edadpromedio FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }    
    public function  actionConsulta5(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT nomequipo, avg(edad) edadpromedio FROM ciclista GROUP BY nomequipo")
              ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, avg(edad) edadpromedio FROM ciclista GROUP BY nomequipo',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'edadpromedio'], 
            "enunciado"=>"Edad media de los ciclistas por equipo",
             "titulo"=>"Consulta 5",
            "sql"=>"SELECT nomequipo, avg(edad) edadpromedio FROM ciclista GROUP BY nomequipo",
        ]);
    }
    public function  actionConsulta5a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo, avg(edad) as edadpromedio")
               ->groupBy("nomequipo"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'edadpromedio'], 
            "enunciado"=>"Edad media de los ciclistas por equipo",
             "titulo"=>"Consulta 5a",
            "sql"=>"SELECT nomequipo, avg(edad) edadpromedio FROM ciclista GROUP BY nomequipo",
        ]);
    }    
    public function  actionConsulta6(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'total'], 
            "enunciado"=>"Ciclistas por equipo",
             "titulo"=>"Consulta 6",
            "sql"=>"SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo",
        ]);
    
    }

    public function  actionConsulta6a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo, count(*) as total")
               ->groupBy("nomequipo"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'total'], 
            "enunciado"=>"Total de ciclistas por equipo",
             "titulo"=>"Consulta 6a",
            "sql"=>"SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo",
        ]);
    }
    public function  actionConsulta7(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) total FROM puerto")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(*) total FROM puerto',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Total de puerto",
             "titulo"=>"Consulta 7",
            "sql"=>"SELECT count(*) total FROM puerto",
        ]);
    
    }
    public function  actionConsulta7a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("count(*) as total"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Total de puertos",
             "titulo"=>"Consulta 7",
            "sql"=>"SELECT count(*) total FROM puerto",
        ]);
    }
    public function  actionConsulta8(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) total FROM puerto where altura>1500")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(*) total FROM puerto where altura > 1500',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Total de puerto > 1500",
             "titulo"=>"Consulta 8",
            "sql"=>"SELECT count(*) total FROM puerto where altura > 1500",
        ]);
    }
    public function  actionConsulta8a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("count(*) as total")
                ->where("altura > 1500"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Total de puertos",
             "titulo"=>"Consulta 8",
            "sql"=>"SELECT count(*) total FROM puerto where altura > 1500",
        ]);
    }
    public function  actionConsulta9(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo having total > 4")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo having total > 4',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'total'], 
            "enunciado"=>"Ciclistas por equipo que tengan mas de cuatro corredores",
             "titulo"=>"Consulta 9",
            "sql"=>"SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo having total > 4",
        ]);
    
    }

    public function  actionConsulta9a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo, count(*) as total")
               ->groupBy("nomequipo")
                ->having("total >4"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'total'], 
            "enunciado"=>"Total de ciclistas por equipo que tengan mas de cuatro corredores",
             "titulo"=>"Consulta 9",
            "sql"=>"SELECT nomequipo, count(*) total FROM ciclista GROUP BY nomequipo HAVING > 4",
        ]);
    }
    public function  actionConsulta10(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT nomequipo, count(*) total FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING total > 4")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nomequipo, count(*) total FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING total > 4',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'total'], 
            "enunciado"=>"Ciclistas por equipo que tengan mas de cuatro corredores",
             "titulo"=>"Consulta 10",
            "sql"=>"SELECT nomequipo, count(*) total FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo having total > 4",
        ]);
    
    }
    public function  actionConsulta10a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("nomequipo, count(*) as total")
                ->where(["between", "edad", '28', '32'])
                ->groupBy("nomequipo")
                ->having("total >4"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo', 'total'], 
            "enunciado"=>"Total de ciclistas por equipo que tengan mas de cuatro corredores",
             "titulo"=>"Consulta 10",
            "sql"=>"SELECT nomequipo, count(*) total FROM ciclista WHERE edad BETWEEN 28 AND 32  GROUP BY nomequipo HAVING > 4",
        ]);
    }
    public function  actionConsulta11(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'total'], 
            "enunciado"=>"Etapas ganada por cada ciclista",
             "titulo"=>"Consulta 11",
            "sql"=>"SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal",
        ]);
    
    }
    public function  actionConsulta11a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("dorsal, count(*) as total")
                ->groupBy("dorsal"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'total'], 
            "enunciado"=>"Etapas ganada por cada ciclista",
             "titulo"=>"Consulta 11",
            "sql"=>"SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal",
        ]);
    }
    public function  actionConsulta12(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal HAVING total > 1")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal HAVING total >1',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'total'], 
            "enunciado"=>"Ciclistas que han ganado mas de una etapa",
             "titulo"=>"Consulta 12",
            "sql"=>"SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal HAVING total > 1",
        ]);
    
    }
    public function  actionConsulta12a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select("dorsal, count(*) as total")
                ->groupBy("dorsal")
                ->having("total >1"),
            
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'total'], 
            "enunciado"=>"Ciclistas que han ganado mas de una etapa",
             "titulo"=>"Consulta 12",
            "sql"=>"SELECT dorsal, count(*) total FROM etapa GROUP BY dorsal HAVING total > 1",
        ]);
    }
    }