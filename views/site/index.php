<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección!</h1>
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">
         <div class="row">
             <!--
             Boton de consulta
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 1</h3>
                    <p>Numero de ciclistas que hay</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta1a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <!--
             Boton de consulta dos
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 2</h3>
                    <p>Numero de ciclistas del equipo Banesto </p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta2a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             
             <!--
             Boton de consulta tres
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 3</h3>
                    <p>Edad media de los ciclistas</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta3a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div>   
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 4</h3>
                    <p>Edad media de los ciclistas de Banesto</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta4a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
               <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 5</h3>
                    <p>Edad media de los ciclistas de todos los equipos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta5a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 6</h3>
                    <p>Numero de ciclistas por equipo</p>
                    <?= Html::a('Active record', ['site/consulta6a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 7</h3>
                    <p>Numero total de puerto</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta7a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 8</h3>
                    <p>Numero total de puertos mayores a 1500</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta8a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 9</h3>
                    <p>Nombre de los equipos que tengan mas de cuatro ciclistas</p>
                    <?= Html::a('Active record', ['site/consulta9a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 10</h3>
                    <p>Nombre de los equipos que tengan mas de cuatro ciclistas cuya edad es entre 28 y 32</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta10a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 11</h3>
                    <p>Indicar el numero de estapas que ha ganado cada ciclista</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta11a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 12</h3>
                    <p>Indicar el dorsal de los clclistas que han ganado mas de una etapa</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta12a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta12'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
        </div> 
    </div>
</div>
